$(function() {
  $(".hashmask").each(function(){
    $(this).hashmask();
    console.log($(this).attr('id')+'-jquery-hashmask-sparkline');
    $('#'+$(this).attr('id')+'-jquery-hashmask-sparkline').css('z-index', 999);

    // Fade out the HashMask when the user leaves this password field
    $(this).blur(function(e){
      var ver = jQuery.fn.jquery;
      if (ver.search('1.3') == 0) {
        $('#'+$(this).attr('id')+'-jquery-hashmask-sparkline').fadeOut();
      }
      else if (ver.search('1.4') == 0 || ver.search('1.5') == 0 || ver.search('1.6') == 0) {
        $('#'+$(this).attr('id')+'-jquery-hashmask-sparkline').delay(3000).fadeOut();
      }
      
    });
    $(this).focus(function(e){
      $('#'+$(this).attr('id')+'-jquery-hashmask-sparkline').show();
    });
  });
});