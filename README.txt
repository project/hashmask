hashmask.module
Brandon Shi
b@bunnylaundering.com

*********************
* Installation
*********************
1. Download the HashMask jQuery library from:
http://code.google.com/p/hashmask/downloads/list
or https://github.com/bran/hashmask
2. Place the HashMask jQuery library in either sites/libraries/hashmask, sites/<your_subsite>/libraries/hashmask or <path_to_the_hashmask_module>/libraries/hashmask so that the 

*********************
* Introduction
*********************
HashMask is an attempt to find a more secure middle ground between clear and masked passwords. It does this by visualizing a hashed representation of the password as a sparkline with color – the intent being that the user would become familiar with this image and be able to easily confirm that they typed the right (or wrong) password.

*********************
* Why?
*********************
My original experiment, HalfMask, produced lots of good discussion, which is exactly what I had hoped for. As security expert Bruce Schneier said recently, password masking is not a panacea. Finding a solution that provides both security and usability is the goal.

As with HalfMask, HashMask is purely an experiment. I’m not suggesting that this is the best middle ground between clear and masked passwords. I am just hoping to get the community thinking about different approaches.

*********************
* How does it work?
*********************
HashMask is a jQuery plugin that will produce a unique and non reversible visualization of a users password. The hope being that they would be able to confirm that they entered their password correctly, but no one else would. It also degrades gracefully so that users without javascript or a poor browser (IE6) will just see a password field.

Technically speaking, it uses a subset of the sha1 hash of the password as the seed for the sparkline’s shape and color. It should be relatively safe from reverse engineering as a result. There is the potential to estimate a possible range of characters of the first section of the hash, but overall this should be a extremely low risk.

<strong>Original creator:</strong> http://lab.arc90.com/2009/07/09/hashmask-another-more-secure-experiment-in-password-masking/