<?php

/**
 * Administration settings form.
 *
 * @see system_settings_form()
 */
function hashmask_admin_settings() {
  $form['nothing'] = array(
    '#value' => 'There is nothing to configure.',
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );
}